package rnyang.jumper;

import java.util.LinkedList;
import java.util.Stack;

public class Game implements Piece {
	
	int[][] board;
	int[][] eval_board;
	int size;
	int player;
	int opponent;
	
	public static final int ROW = 0;
	public static final int COL = 1;
	
 	public Game (int n, int p) 
 	{
		this.size = n;
		board = new int[n][n];
		for (int row = 0; row < n; row++){
			for (int column = 0; column < n; column++) {
				board[row][column] = EMPTY;
				eval_board[row][column] = 0;
			}
		}
		this.player = p;
		if (this.player == WHITE)
			this.opponent = BLACK;
		else
			this.opponent = WHITE;
	}
 	public Move gameSearch(int maxDepth, int agent)
 	{
 		LinkedList<Integer> moves_score = new LinkedList<Integer>();
 		LinkedList<Move> moves = generateMoves(player);
 		if (agent == 0){
 			this.naiveMinimax(moves_score, maxDepth, true, maxDepth);
 		}else if(agent == 1){
 			this.naiveNegamax(moves_score, maxDepth, 1, maxDepth);
 		}else if(agent == 2){
 			this.alphaBetaMinimax(moves_score, maxDepth, true, 
 	 				maxDepth,Integer.MIN_VALUE,Integer.MAX_VALUE);
 		}else if(agent == 3){
 			this.alphaBetaNegamax(moves_score, maxDepth, 1,
 					Integer.MIN_VALUE,Integer.MAX_VALUE, maxDepth);
 		}
 		int max_score = moves_score.peekFirst();
 		int max_index = 0;
 		for (Integer score: moves_score){
 			if (score > max_score){
 				max_score = score;
 				max_index = moves_score.indexOf(max_score);
 			}
 		}
 		return moves.get(max_index);
 	}
 	
 	public int naiveMinimax(LinkedList<Integer> moves_score,
 			int depth, boolean max, int maxDepth)
 	{
 		if (depth == 0 || this.terminalState())
 			return this.evaluate();
 		
 		if (max){
 			int bestVal = Integer.MIN_VALUE;
 			LinkedList<Move> moves = generateMoves(player);
 			for (Move m:moves){
 				updateGameState(m);
 				int val = naiveMinimax(moves_score,depth-1, false, maxDepth);
 				bestVal = Math.max(bestVal, val);
 				revertGameState(m);
 				if (depth == maxDepth){
 					moves_score.add(bestVal);
 				}
 			}
 			return bestVal;
 		}else{
 			int bestVal = Integer.MAX_VALUE;
 			LinkedList<Move> moves = generateMoves(opponent);
 			for (Move m:moves){
 				updateGameState(m);
 				int val = naiveMinimax(moves_score,depth-1, true, maxDepth);
 				bestVal = Math.min(bestVal, val);
 				revertGameState(m);
 			}
 			return bestVal;
 		}
 	}
 	
 	public int alphaBetaMinimax(LinkedList<Integer> moves_score,
 			int depth, boolean max, int maxDepth, int alpha, int beta)
 	{
 		if (depth == 0 || this.terminalState())
 			return this.evaluate();
 		
 		if (max){
 			LinkedList<Move> moves = generateMoves(player);
 			for (Move m:moves){
 				updateGameState(m);
 				alpha = Math.max(alpha,alphaBetaMinimax(moves_score,depth-1, false, 
 						maxDepth,alpha,beta));
 				revertGameState(m);
 				if (depth == maxDepth){
 					moves_score.add(alpha);
 				}
 				if (beta<= alpha){
 					break;
 				}
 			}
 			return alpha;
 		}else{
 			LinkedList<Move> moves = generateMoves(opponent);
 			for (Move m:moves){
 				updateGameState(m);
 				beta = Math.min(beta, alphaBetaMinimax(moves_score,depth-1, true, 
 						maxDepth,alpha,beta));
 				revertGameState(m);
 				if (beta<= alpha){
 					break;
 				}
 			}
 			return beta;
 		}
 	}
 	public int naiveNegamax(LinkedList<Integer> moves_score,int depth, int color, int maxDepth){
 		if (depth == 0 || this.terminalState())
 			return (this.evaluate()*color);
 		int bestVal = Integer.MIN_VALUE;
 		int p = color == 1 ? player : opponent;
 		LinkedList<Move> moves = generateMoves(p);
 		for (Move m:moves){
 			updateGameState(m);
 			int val = -naiveNegamax(moves_score,depth-1,-color,maxDepth);
 			bestVal = Math.max(bestVal, val);
 			revertGameState(m);
 			if (depth == maxDepth){
				moves_score.add(bestVal);
			}
 		}
 		return bestVal;
 	}
 	public int alphaBetaNegamax(LinkedList<Integer> moves_score,int depth, int color, 
 			int alpha,int beta, int maxDepth){
 		if (depth == 0 || this.terminalState())
 			return (this.evaluate()*color);
 		int bestVal = Integer.MIN_VALUE;
 		int p = color == 1 ? player : opponent;
 		LinkedList<Move> moves = generateMoves(p);
 		for (Move m:moves){
 			updateGameState(m);
 			int val = -alphaBetaNegamax(moves_score,depth-1,-color,-beta,-alpha,maxDepth);
 			bestVal = Math.max(bestVal, val);
 			alpha = Math.max(alpha, val);
 			revertGameState(m);
 			if (depth == maxDepth){
				moves_score.add(bestVal);
			}
 			if (alpha >= beta){
				break;
			}
 		}
 		return bestVal;
 	}
 	
 	public LinkedList<Move> generateMoves(int p)
	{
		LinkedList<Move> moves = new LinkedList<Move>();
		LinkedList<Move> placeMoves = new LinkedList<Move>();
		LinkedList<Move> jumpMoves = new LinkedList<Move>();
		
		for (int r = 0; r < this.size; r++) {
			for (int c = 0; c < this.size; c++) 
			{
				if (this.board[r][c] == EMPTY)
					placeMoves.add(new Move(p, r, c));
				
				if (this.board[r][c] == p)
				{
					LinkedList<int[]> path = new LinkedList<int[]>();
					jumpMoves = jumpSearch(arrayFormat(r, c), p, path, arrayFormat(r, c), jumpMoves);
				}
			}
		}
		moves.addAll(jumpMoves);
		moves.addAll(placeMoves);
		return moves;
	}
 	
	public LinkedList<Move> jumpSearch(int[]dest, int p, LinkedList<int[]> path, 
			int[] branch, LinkedList<Move> jumpMoves)
	{
		path.add(dest);
		Stack<int[]> jumpToLocs = this.checkSurroundingMoves(dest[ROW], dest[COL]);
		
		while(!jumpToLocs.empty())
		{
			int[] piece = jumpToLocs.pop();
			
			this.updateGameState(dest[ROW], dest[COL], piece[ROW], piece[COL], p);
			if(!jumpToLocs.empty())
				jumpMoves = this.jumpSearch(piece, p, path, dest, jumpMoves);
			else
				jumpMoves = this.jumpSearch(piece, p, path, branch, jumpMoves);
		}
		
		if(path.size() > 1 ){
			jumpMoves.add(this.traceBackJump(branch, path, p));
		}
		
		return jumpMoves;
	}
	
	public Move traceBackJump(int[] junction, LinkedList<int[]> path, int p)
	{
		int[] row = new int[path.size()];
		int[] col = new int[path.size()];
		int i = 0;
		for (int[] piece : path)
		{
			row[i] = piece[ROW];
			col[i] = piece[COL];
			i++;
		}

		while((path.peekLast()[ROW] != junction[ROW]) || (path.peekLast()[COL] != junction[COL]))
		{
			int[] changePiece = path.removeLast();
			int[] originPiece = path.peekLast();
			revertGameState(originPiece[ROW],originPiece[COL], changePiece[ROW],changePiece[COL],p);
		}
		return new Move(p, row, col);
	}
	
	//complete
	public void revertGameState(Move m)
	{
		if(m.IsPlaceMove)
		{
			this.board[m.RowPositions[0]][m.ColPositions[0]] = EMPTY;
		} else 
		{
			int enemy = m.P == WHITE ? BLACK : WHITE;
			
			int midR, midC;
			for (int i = 1; i < m.RowPositions.length; i++)
			{
				midR = (m.RowPositions[i-1] + m.RowPositions[i]) / 2;
				midC = (m.ColPositions[i-1] + m.ColPositions[i]) / 2;
				if (this.board[midR][midC] == DEAD){
					this.board[midR][midC] = enemy;
				}
				this.board[m.RowPositions[i]][m.ColPositions[i]] = EMPTY;
			}
		}
	}
	
	public void revertGameState(int r0, int c0, int r1, int c1, int p)
	{
		int midR = (r0 + r1) / 2;
		int midC = (c0 + c1) / 2;
		int enemy = p == WHITE ? BLACK : WHITE;
		if (this.board[midR][midC] == DEAD){
			this.board[midR][midC] = enemy;
		}
		this.board[r1][c1] = EMPTY;
	}
	//complete
	public void updateGameState(Move m)
	{
		if (m.IsPlaceMove)
		{
			this.board[m.RowPositions[0]][m.ColPositions[0]] = m.P;
		}else
		{
			int midR, midC;
			for (int i = 1; i < m.RowPositions.length; i++)
			{
				midR = (m.RowPositions[i-1] + m.RowPositions[i]) / 2;
				midC = (m.ColPositions[i-1] + m.ColPositions[i]) / 2;
				if (this.board[midR][midC] != EMPTY && this.board[midR][midC] != m.P){
					this.board[midR][midC] = DEAD;
				}
				this.board[m.RowPositions[i]][m.ColPositions[i]] = m.P;
			}
		}
	}

	public void updateGameState(int r0, int c0, int r1, int c1, int p)
	{
		int midR = (r0 + r1) / 2;
		int midC = (c0 + c1) / 2;
		int enemy = p == WHITE ? BLACK : WHITE;
		if (this.board[midR][midC] == enemy){
			this.board[midR][midC] = DEAD;
		}
		this.board[r1][c1] = p;
	}
	
	//complete
	public Stack<int[]> checkSurroundingMoves(int x, int y) 
	{
		Stack<int[]> possibleJumps = new Stack<int[]>();

		// Top - Right
		if (Game.inBound(size,x+2,y+2) && (board[x+2][y+2] == EMPTY) && 
				(board[x+1][y+1] == WHITE || board[x+1][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x+2), (y+2)));
		
		// Top
		if (Game.inBound(size,x,y+2) && (board[x][y+2] == EMPTY) && 
				(board[x][y+1] == WHITE || board[x][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x), (y+2)));
				
		// Top - Left
		if (Game.inBound(size,x-2,y+2) && (board[x-2][y+2] == EMPTY) && 
				(board[x-1][y+1] == WHITE || board[x-1][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y+2)));
		
		// Right
		if (Game.inBound(size,x+2,y) && (board[x+2][y] == EMPTY) && 
				(board[x+1][y] == WHITE || board[x+1][y] == BLACK))
			possibleJumps.add(arrayFormat((x+2),(y)));
		
		// Left
		if (Game.inBound(size,x-2,y) && (board[x-2][y] == EMPTY) && 
				(board[x-1][y] == WHITE || board[x-1][y] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y)));
		
		// Bottom - Right
		if (Game.inBound(size,x+2,y-2) && (board[x+2][y-2] == EMPTY) && 
				(board[x+1][y-1] == WHITE || board[x+1][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x+2),(y-2)));
		
		// Bottom
		if (Game.inBound(size,x,y-2) && (board[x][y-2] == EMPTY) && 
				(board[x][y-1] == WHITE || board[x][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x), (y-2)));
		
		// Bottom - Left
		if (Game.inBound(size,x-2,y-2) && (board[x-2][y-2] == EMPTY) && 
				(board[x-1][y-1] == WHITE || board[x-1][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y-2)));

		return possibleJumps;
	}
	//complete
	
	public static boolean inBound(int size, int locX,int locY)
	 {
		 return ((locX >= 0 && locY >= 0) && (locX < size && locY < size));
	 }
	
	//complete
	public static int[] arrayFormat(int x, int y)
	{
		int[] loc = new int[2];
		loc[ROW] = x;
		loc[COL] = y;
		return loc;
	}
	// Naive Evaluation
	public int basicEvaluate()
	{
		int score = 0;
		for (int r = 0; r < board.length; r++) {
			for (int c = 0; c < board.length; c++)
			{
				if (board[r][c] == player)
					score++;
				if (board[r][c] == opponent)
					score--;
			}
		}
		return score;
	}
	public int evaluate()
	{
		int corner_v = 0;
		int corner_v1 = this.board.length -1;
		int oppo;
		int player_sum = 0;
		int oppo_sum = 0;
		int corner_w = 13;
		int edge_w = 12;
		int cent_w = 11;
		if (player == Piece.WHITE){
			oppo = Piece.BLACK;
		}else{
			oppo = Piece.WHITE;
		}
		for (int row = 0; row < this.board.length; row++){
			for (int col = 0; col < this.board.length; col++){
				if (this.board[row][col] == player){
					if ((row == corner_v && col == corner_v)
							|| (row == corner_v && col == corner_v1) 
							|| (row == corner_v1 && col == corner_v)
							|| (row == corner_v1 && col == corner_v1)){
						player_sum += corner_w;
					}else if(row == corner_v || row == corner_v1
							|| col == corner_v || col == corner_v1){
						player_sum += edge_w;
					}else{
						player_sum += cent_w;
					}	
				}else if (this.board[row][col] == oppo){
					if ((row == corner_v && col == corner_v)
							|| (row == corner_v && col == corner_v1) 
							|| (row == corner_v1 && col == corner_v)
							|| (row == corner_v1 && col == corner_v1)){
						oppo_sum += corner_w;
					}else if(row == corner_v || row == corner_v1
							|| col == corner_v || col == corner_v1){
						oppo_sum += edge_w;
					}else{
						oppo_sum += cent_w;
					}
				}
			}
		}
		return (player_sum - oppo_sum);
	}
	
	public boolean terminalState(){
		for (int row=0 ; row < board.length; row++){
			for (int col=0; col < board.length; col++){
				if (board[row][col] == EMPTY){
					return false;
				}
			}
		}
		return true;
	}
}
