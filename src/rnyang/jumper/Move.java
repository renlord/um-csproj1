package rnyang.jumper;

import rnyang.jumper2.Piece;
/*
 *   Move:
 *      Which can be a place or a jump
 *      
 *   @author msalehi
 *   
 */

public class Move implements Piece {
	public int P;
	public boolean IsPlaceMove;
	public int RowPositions[];
	public int ColPositions[];
	
	public Move()
	{
		P = EMPTY;
		IsPlaceMove = true;
		//RowPositions = ;
		//ColPositions = ;		
		
	}
	
	/**
	 * 
	 * @param player
	 * @param r
	 * @param c
	 */
	public Move(int player, int[] r, int[] c)
	{
		P = player;
		IsPlaceMove = false;
		RowPositions = r;
		ColPositions = c;	
	}
	
	/**
	 * 
	 * @param player
	 * @param r
	 * @param c
	 */
	public Move(int player, int r, int c)
	{
		P = player;
		IsPlaceMove = true;
		RowPositions = new int[1];
		ColPositions = new int[1];
		RowPositions[0] = r;
		ColPositions[0] = c;
	}
	
}