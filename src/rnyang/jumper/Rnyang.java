package rnyang.jumper;

import java.io.PrintStream;

public class Rnyang implements Player, Piece {

	Game g;
	static int instanceCount = 0;
	
	static final int maxInstanceCount = 2;
	
	@Override
	public int getWinner() {
		int wCount = 0, bCount = 0, eCount = 0;
		for (int r = 0; r < this.g.size; r++) {
			for (int c = 0; c < this.g.size; c++) {
				if(g.board[r][c] == WHITE)
					wCount++;
				if(g.board[r][c] == BLACK)
					bCount++;
				if(g.board[r][c] == EMPTY)
					eCount++;
			}
		}
		
		if (eCount > 0)
			return EMPTY;
		else if (bCount > wCount)
			return BLACK;
		else if (wCount > bCount)
			return WHITE;
		else if (wCount == bCount)
			return DEAD;
		else
			return INVALID;
		
	}

	@Override
	public int init(int n, int p) {
		if (instanceCount < maxInstanceCount)
			instanceCount++;
		else
			return -1;
		g = new Game(n, p);
		return 0;
	}

	@Override
	public Move makeMove() {
		Move m = g.gameSearch(5,2);
		g.updateGameState(m);
		return m;
	}

	@Override
	public int opponentMove(Move m) {

		// Initial Checks
		if (m.RowPositions.length != m.ColPositions.length || 
				m.P != g.opponent)
			return INVALID;
			
		
		if (m.IsPlaceMove)
		{
			
			// check legit place move?
			if (m.RowPositions.length != 1 ||
					g.board[m.RowPositions[0]][m.ColPositions[0]] != EMPTY)
				return INVALID;
			// update board
			g.board[m.RowPositions[0]][m.ColPositions[0]] = m.P;
		} else
		{
			// check jump moves legit?
			if(g.board[m.RowPositions[0]][m.ColPositions[0]] != m.P)
				return INVALID;
			
			int midR, midC;
			
			for (int i = 1; i < m.RowPositions.length; i++)
			{
				midR = (m.RowPositions[i] + m.RowPositions[i-1]) / 2;
				midC = (m.ColPositions[i] + m.ColPositions[i-1]) / 2;
				if (g.board[midR][midC] != WHITE && g.board[midR][midC] != BLACK)
					return INVALID;
				if(g.board[m.RowPositions[i]][m.ColPositions[i]] != EMPTY)
					return INVALID;
				// update
				if (g.board[midR][midC] != m.P)
					g.board[midR][midC] = DEAD;
				g.board[m.RowPositions[i]][m.ColPositions[i]] = m.P;
			}
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		for (int i = 0; i < g.size; i++) {
			for (int k = 0; k < g.size; k++) {
				if (g.board[i][k] == WHITE)
					output.print("W ");
				else if (g.board[i][k] == BLACK)
					output.print("B ");
				else if (g.board[i][k] == EMPTY)
					output.print("- ");
				else if (g.board[i][k] == DEAD)
					output.print("X ");
				else
					output.print("? ");
			}
			System.out.print("\n");
		}
		//System.out.println(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	}

}
