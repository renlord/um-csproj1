package rnyang.jumper2;

import java.util.LinkedList;
import java.util.Stack;

public class Game implements Piece{

	// Typical Game Instance Variables.
	int board[][];
	int size;
	int player;
	int opponent;
	
	// Current Game State hash;
	long currentZobristHash;
	
	// Zobrist Transposition Table
	Zobrist zobrist;
	
	static final int ROW = 0;
	static final int COL = 1;
	
	public Game(int n, int p)
	{
		this.board = new int[n][n];
		
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) 
			{
				this.board[r][c] = EMPTY;
			}
		}
		this.player = p;
		this.opponent = (p == WHITE) ? BLACK : WHITE;
		this.size = n;
		
		// Zobrist
		this.zobrist = new Zobrist(n);
		this.currentZobristHash = zobrist.init();

	}
	
	//complete
	public LinkedList<Move> generateMoves(int p)
	{
		LinkedList<Move> moves = new LinkedList<Move>();
		LinkedList<Move> placeMoves = new LinkedList<Move>();
		LinkedList<Move> jumpMoves = new LinkedList<Move>();
		
		for (int r = 0; r < this.size; r++) {
			for (int c = 0; c < this.size; c++) 
			{
				if (this.board[r][c] == EMPTY)
					placeMoves.add(new Move(p, r, c));
				
				if (this.board[r][c] == p)
				{
					LinkedList<int[]> path = new LinkedList<int[]>();
					int[] origin = arrayFormat(r,c);
					jumpMoves = this.jumpSearch(origin, p, path, origin, jumpMoves);
				}
			}
		}
		moves.addAll(jumpMoves);
		moves.addAll(placeMoves);
		return moves;
	}
	
	//complete
	public LinkedList<Move> jumpSearch(int[]dest, int p, LinkedList<int[]> path, 
			int[] branch, LinkedList<Move> jumpMoves)
	{
		path.add(dest);
		Stack<int[]> jumpToLocs = this.checkSurroundingMoves(dest[ROW], dest[COL]);
		
		while(!jumpToLocs.empty())
		{
			int[] piece = jumpToLocs.pop();
			
			this.updateGameState(dest[ROW], dest[COL], piece[ROW], piece[COL], p);
			if(!jumpToLocs.empty())
				jumpMoves = this.jumpSearch(piece, p, path, dest, jumpMoves);
			else
				jumpMoves = this.jumpSearch(piece, p, path, branch, jumpMoves);
		}
		
		if(path.size() > 1)
			jumpMoves.add(this.traceBackJump(branch, path, p));
		
		return jumpMoves;
	}
	
	//complete
	public Move traceBackJump(int[] junction, LinkedList<int[]> path, int p)
	{
		int[] row = new int[path.size()];
		int[] col = new int[path.size()];
		int opponent = (p == WHITE) ? BLACK : WHITE;
		int i = 0;
		for (int[] piece : path)
		{
			row[i] = piece[ROW];
			col[i] = piece[COL];
			i++;
		}
		
		while((path.peekLast()[0] != junction[0]) || (path.peekLast()[1] != junction[1]))
		{
			int[] changePiece = path.removeLast();
			int[] originPiece = path.peekLast();
			int midR = (changePiece[ROW] + originPiece[ROW]) / 2;
			int midC = (changePiece[COL] + originPiece[COL]) / 2;
			if (this.board[midR][midC] == DEAD)
				this.board[midR][midC] = opponent;
			
			this.board[changePiece[ROW]][changePiece[COL]] = EMPTY;
		}
		
		return new Move(p, row, col);
	}
	
	//complete
	public void revertGameState(Move m)
	{
		if(m.IsPlaceMove)
		{
			this.board[m.RowPositions[0]][m.ColPositions[0]] = EMPTY;
		} else 
		{
			int enemy = (m.P == WHITE) ? BLACK : WHITE;
			
			int midR, midC;
			for (int i = 1; i < m.RowPositions.length; i++)
			{
				midR = (m.RowPositions[i-1] + m.RowPositions[i]) / 2;
				midC = (m.ColPositions[i-1] + m.ColPositions[i]) / 2;
				if (this.board[midR][midC] == DEAD)
					this.board[midR][midC] = enemy;
				
				this.board[m.RowPositions[i]][m.ColPositions[i]] = EMPTY;
			}
		}
		this.currentZobristHash = this.zobrist.updateGameHash(m, this.currentZobristHash);
	}
	
	//complete
	public void updateGameState(Move m)
	{
		int opponent = (m.P == WHITE) ? BLACK : WHITE;
		
		if (m.IsPlaceMove)
		{
			this.board[m.RowPositions[0]][m.ColPositions[0]] = m.P;
		} else
		{
			int midR, midC;
			for (int i = 1; i < m.RowPositions.length; i++)
			{
				midR = (m.RowPositions[i-1] + m.RowPositions[i]) / 2;
				midC = (m.ColPositions[i-1] + m.ColPositions[i]) / 2;
				if (this.board[midR][midC] == opponent)
					this.board[midR][midC] = DEAD;
				
				this.board[m.RowPositions[i]][m.ColPositions[i]] = m.P;
			}
		}
		this.currentZobristHash = this.zobrist.updateGameHash(m, this.currentZobristHash);
	}

	//complete
	public void updateGameState(int r0, int c0, int r1, int c1, int p)
	{
		int opponent = (p == WHITE) ? BLACK : WHITE;
		
		int midR = (r0 + r1) / 2;
		int midC = (c0 + c1) / 2;
		if(this.board[midR][midC] == opponent)
		{
			this.currentZobristHash ^= this.zobrist.zobrist[midR][midC][opponent];
			this.board[midR][midC] = DEAD;
			this.currentZobristHash ^= this.zobrist.zobrist[midR][midC][DEAD];
		}
		this.board[r1][c1] = p;
	}
	
	//complete
	public boolean terminalGame()
	{
		for (int r = 0; r < this.size; r++) {
			for (int c = 0; c < this.size; c++) 
			{
				if (this.board[r][c] == EMPTY) return false;
					
			}
		}
		return true;
	}
	
	//complete
	public Stack<int[]> checkSurroundingMoves(int x, int y) 
	{
		Stack<int[]> possibleJumps = new Stack<int[]>();

		// Top - Right
		if (Game.inBound(size,x+2,y+2) && (board[x+2][y+2] == EMPTY) && 
				(board[x+1][y+1] == WHITE || board[x+1][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x+2), (y+2)));
		
		// Top
		if (Game.inBound(size,x,y+2) && (board[x][y+2] == EMPTY) && 
				(board[x][y+1] == WHITE || board[x][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x), (y+2)));
				
		// Top - Left
		if (Game.inBound(size,x-2,y+2) && (board[x-2][y+2] == EMPTY) && 
				(board[x-1][y+1] == WHITE || board[x-1][y+1] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y+2)));
		
		// Right
		if (Game.inBound(size,x+2,y) && (board[x+2][y] == EMPTY) && 
				(board[x+1][y] == WHITE || board[x+1][y] == BLACK))
			possibleJumps.add(arrayFormat((x+2),(y)));
		
		// Left
		if (Game.inBound(size,x-2,y) && (board[x-2][y] == EMPTY) && 
				(board[x-1][y] == WHITE || board[x-1][y] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y)));
		
		// Bottom - Right
		if (Game.inBound(size,x+2,y-2) && (board[x+2][y-2] == EMPTY) && 
				(board[x+1][y-1] == WHITE || board[x+1][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x+2),(y-2)));
		
		// Bottom
		if (Game.inBound(size,x,y-2) && (board[x][y-2] == EMPTY) && 
				(board[x][y-1] == WHITE || board[x][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x), (y-2)));
		
		// Bottom - Left
		if (Game.inBound(size,x-2,y-2) && (board[x-2][y-2] == EMPTY) && 
				(board[x-1][y-1] == WHITE || board[x-1][y-1] == BLACK))
			possibleJumps.add(arrayFormat((x-2), (y-2)));

		return possibleJumps;
	}
	
	//complete
	public static boolean inBound(int size, int locX,int locY)
	 {
		 return ((locX >= 0 && locY >= 0) && (locX < size && locY < size));
	 }

	//complete
	public static int[] arrayFormat(int x, int y)
	{
		int[] loc = new int[2];
		loc[ROW] = x;
		loc[COL] = y;
		return loc;
	}

}
