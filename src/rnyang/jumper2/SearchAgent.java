package rnyang.jumper2;

import java.util.LinkedList;

/*
 * Compilation of Search Algorithms we have implemented to search.
 * Compilation of Evaluation Functions and Machine Learning algorithms to assist
 * in evaluation of game states.
 */
public class SearchAgent implements Piece {

	// Naive Minimax
	
	// Minimax + AlphaBetaPruning
	public static Move alphaBetaMinimax(Game g, int depth)
	{
		LinkedList<Move> moves = g.generateMoves(g.player);
		Move bestMove = null;
		
		int a = Integer.MIN_VALUE;
		int b = Integer.MAX_VALUE;
		int prev = Integer.MIN_VALUE;
		
		for (Move m : moves)
		{	
			g.updateGameState(m);
			a = Math.max(a, minMove(g, depth-1, a, b));
			if(prev != a) {
				bestMove = m;
				prev = a;
			}
			g.revertGameState(m);
		}
		return bestMove;
	}
	
	public static int maxMove(Game g, int d, int a, int b)
	{		
		if (d == 0 || g.terminalGame())
			return basicEvaluate(g);
			
		LinkedList<Move> moves = g.generateMoves(g.player);
		
		for (Move m : moves)
		{
			g.updateGameState(m);
			a = Math.max(a, minMove(g, d-1, a, b));
			g.revertGameState(m);
			if (b <= a)
				break;
		}
		return a;
	}
	
	public static int minMove(Game g, int d, int a, int b)
	{
		if (d == 0 || g.terminalGame())
			return basicEvaluate(g);
		
		LinkedList<Move> moves = g.generateMoves(g.opponent);
		
		for (Move m : moves)
		{
			g.updateGameState(m);
			b = Math.min(b, maxMove(g, d-1, a, b));
			g.revertGameState(m);
			if(b <= a)
				break;
		}
		return b;
	}
	
	// Minimax + AlphaBetaPruning + Transposition Tables
	public static Move ABTTMinmax(Game g, int depth)
	{
		LinkedList<Move> moves = g.generateMoves(g.player);
		Move bestMove = null;
		
		int a = Integer.MIN_VALUE;
		int b = Integer.MAX_VALUE;
		int prev = Integer.MIN_VALUE;
		
		for (Move m : moves)
		{	
			g.updateGameState(m);
			a = Math.max(a, ABTTmin(g, depth-1, a, b));
			if(prev != a) {
				bestMove = m;
				prev = a;
			}
			g.revertGameState(m);
			g.zobrist.resetTable();
		}
		g.zobrist.resetTable();
		return bestMove;
	}
	
	public static int ABTTmax(Game g, int d, int a, int b)
	{
		if (d == 0 || g.terminalGame())
		{	
			int score = basicEvaluate(g);
			g.zobrist.updateHashNode(g.currentZobristHash, d, score, Zobrist.HashNode.EXACT);
			return score;
		}
		
		LinkedList<Move> moves = g.generateMoves(g.player);
		
		for (Move m : moves)
		{
			g.updateGameState(m);
			if (g.zobrist.queryHash(g.currentZobristHash, d))
			{
				Zobrist.HashNode hashNode = g.zobrist.getHashNode(g.currentZobristHash);
				switch(hashNode.nodeType)
				{
					case Zobrist.HashNode.ALPHA:
					{
						if (a < hashNode.score)
							a = hashNode.score;
						break;
					}
					case Zobrist.HashNode.BETA:
					{
						if (b > hashNode.score)
							b = hashNode.score;
						break;
					}
					case Zobrist.HashNode.EXACT:
					{
						g.revertGameState(m);
						return hashNode.score;
					}
				}
			}
			a = Math.max(a, ABTTmin(g, d-1, a, b));
			g.zobrist.updateHashNode(g.currentZobristHash, d, a, Zobrist.HashNode.ALPHA);
			g.revertGameState(m);
			
			if (b <= a)
				break;
		}
		return a;
	}
	
	public static int ABTTmin(Game g, int d, int a, int b)
	{
		if (d == 0 || g.terminalGame())
		{	
			int score = basicEvaluate(g);
			g.zobrist.updateHashNode(g.currentZobristHash, d, score, Zobrist.HashNode.EXACT);
			return score;
		}	
		
		LinkedList<Move> moves = g.generateMoves(g.opponent);
		
		for (Move m : moves)
		{
			g.updateGameState(m);
			
			if (g.zobrist.queryHash(g.currentZobristHash, d))
			{
				Zobrist.HashNode hashNode = g.zobrist.getHashNode(g.currentZobristHash);
				switch(hashNode.nodeType)
				{
					case Zobrist.HashNode.ALPHA:
					{
						if (a < hashNode.score)
							a = hashNode.score;
						break;
					}
					case Zobrist.HashNode.BETA:
					{
						if (b > hashNode.score)
							b = hashNode.score;
						break;
					}
					case Zobrist.HashNode.EXACT:
					{
						g.revertGameState(m);
						return hashNode.score;
					}
				}
			}
			b = Math.min(b, ABTTmax(g, d-1, a, b));
			g.zobrist.updateHashNode(g.currentZobristHash, d, b, Zobrist.HashNode.BETA);
			g.revertGameState(m);
			if(b <= a)
				break;
		}
		return b;
	}
	
	// Naive Evaluation
	public static int basicEvaluate(Game g)
	{
		int score = 0;
		for (int r = 0; r < g.size; r++) {
			for (int c = 0; c < g.size; c++)
			{
				if (g.board[r][c] == g.player)
					score += 2;
				if (g.board[r][c] == g.opponent)
					score -= 2;
			}
		}
		return score;
	}
	
	// Heuristic Improved Evaluation Function: Corner Biased. 
	public static int evaluateGameState(Game g)
	{
		int corner_v = 0;
		int corner_v1 = g.board.length -1;
		int oppo = (g.player == WHITE) ? BLACK : WHITE;
		int player_sum = 0;
		int oppo_sum = 0;
		int corner_w = 13;
		int edge_w = 12;
		int cent_w = 11;
		for (int row = 0; row < g.board.length; row++){
			for (int col = 0; col < g.board.length; col++){
				if (g.board[row][col] == g.player){
					if ((row == corner_v && col == corner_v)
							|| (row == corner_v && col == corner_v1) 
							|| (row == corner_v1 && col == corner_v)
							|| (row == corner_v1 && col == corner_v1)){
						player_sum += corner_w;
					}else if(row == corner_v || row == corner_v1
							|| col == corner_v || col == corner_v1){
						player_sum += edge_w;
					}else{
						player_sum += cent_w;
					}	
				}else if (g.board[row][col] == oppo){
					if ((row == corner_v && col == corner_v)
							|| (row == corner_v && col == corner_v1) 
							|| (row == corner_v1 && col == corner_v)
							|| (row == corner_v1 && col == corner_v1)){
						oppo_sum += corner_w;
					}else if(row == corner_v || row == corner_v1
							|| col == corner_v || col == corner_v1){
						oppo_sum += edge_w;
					}else{
						oppo_sum += cent_w;
					}
				}
			}
		}
		return (player_sum - oppo_sum);
	}

	//TD Leaf ML Evaluation
}
