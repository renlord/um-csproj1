package rnyang.jumper2;

import java.util.Random;

public class Zobrist implements Piece {
// an implementation of Zobrist Hashing
// for quick game search.
	
	long[][][] zobrist; 
	int capacity;
	HashNode[] lookupTable;
	final Random hashRand; 
	
	static final int TABLESIZE = 50000;
	static final int HALFTABLE = 25000;
	
	public Zobrist(int capacity)
	{
		zobrist = new long[capacity][capacity][4];
		hashRand = new Random(System.currentTimeMillis());
		this.capacity = capacity;
	}
	
	public long init()
	{	
		long gameStateHash = Long.MIN_VALUE;
		
		for (int r = 0; r < this.capacity; r++) {
			for (int c = 0; c < this.capacity; c++) 
			{
				zobrist[r][c][EMPTY] = hashRand.nextLong();
				zobrist[r][c][WHITE] = hashRand.nextLong();
				zobrist[r][c][BLACK] = hashRand.nextLong();
				zobrist[r][c][DEAD] = hashRand.nextLong();
			}
		}
		
		for (int r = 0; r < this.capacity; r++) {
			for (int c = 0; c < this.capacity; c++) 
			{
				gameStateHash ^= zobrist[r][c][EMPTY];
			}
		}
		
		lookupTable = new HashNode[TABLESIZE];
		
		for (int i = 0; i < TABLESIZE; i++)
			lookupTable[i] = new HashNode(0, 0, 0, 0);
		
		return gameStateHash;
	}
	
	public long updateGameHash(Move m, long gameStateHash)
	{
		if (m.IsPlaceMove) 
		{
			gameStateHash ^= zobrist[m.RowPositions[0]][m.ColPositions[0]][EMPTY];
			gameStateHash ^= zobrist[m.RowPositions[0]][m.ColPositions[0]][m.P];
		} else 
		{
			int midR, midC;
			for (int i = 1; i < m.RowPositions.length; i++) 
			{
				midR = (m.RowPositions[i] + m.RowPositions[i-1]) / 2;
				midC = (m.ColPositions[i] + m.ColPositions[i-1]) / 2;
				// update dead cells.
				gameStateHash ^= zobrist[midR][midC][EMPTY];
				gameStateHash ^= zobrist[midR][midC][DEAD];
				// update new jumpTO locations.
				gameStateHash ^= zobrist[m.RowPositions[i]][m.ColPositions[i]][EMPTY];
				gameStateHash ^= zobrist[m.RowPositions[i]][m.ColPositions[i]][m.P];
			}
		}
		return gameStateHash;
	}
	
	public boolean queryHash(long gameHash, int d)
	{
		int index = (int) (gameHash % HALFTABLE);
		if (index < 0)
			index = Math.abs(index) + HALFTABLE;
		
		return ((lookupTable[index].depth <= d) && (lookupTable[index].hash == gameHash)) ?
				true : false;
	}
	
	public void updateHashNode(long gameHash, int d, int s, int type)
	{
		int index = (int) (gameHash % HALFTABLE);
		if (index < 0)
			index = Math.abs(index) + HALFTABLE;
		
	
		if(lookupTable[index].ancient) 
		{
			lookupTable[index].hash = gameHash;
			lookupTable[index].depth = d;
			lookupTable[index].score = s;
			lookupTable[index].nodeType = type;
			lookupTable[index].ancient = false;
		}

	}
	
	public void resetTable()
	{
		for(int i = 0; i < TABLESIZE; i++)
			this.lookupTable[i].ancient = true;
	}
	
	public HashNode getHashNode(long gameHash)
	{
		int index = (int) (gameHash % HALFTABLE);
		if (index < 0)
			index = Math.abs(index) + HALFTABLE;
		
		return lookupTable[index];
	}
	
	public class HashNode implements Piece {
		
		long hash;
		int depth, nodeType;
		int score;
		boolean ancient;
		
		//size 18 
		static final int ALPHA = 0;
		static final int BETA = 1;
		static final int EXACT = 2;
		
		public HashNode(long zobristHash, int depth, int score, int nodeType)
		{
			this.hash = zobristHash;
			this.depth = depth;
			this.score = score;
			this.nodeType = nodeType;
			this.ancient = true;
		}
	}
		
}
